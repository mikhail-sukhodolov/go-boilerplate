package storage

import (
	"boilerplate/internal/db/adapter"
	"boilerplate/internal/models"
	"context"
)

type Userer interface {
	Create(ctx context.Context, u models.UserDTO) (int, error)
	Update(ctx context.Context, u models.UserDTO) error
	GetByID(ctx context.Context, userID int) (models.UserDTO, error)
	GetByIDs(context.Context, []int) ([]models.UserDTO, error)
	GetByEmail(ctx context.Context, email string) (models.UserDTO, error)
	GetByFilter(context.Context, adapter.Condition) ([]models.UserDTO, error)
}
