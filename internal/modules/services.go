package modules

import (
	"boilerplate/internal/infrastructure/component"
	aservice "boilerplate/internal/modules/auth/service"
	uservice "boilerplate/internal/modules/user/service"
	"boilerplate/internal/storages"
)

type Services struct {
	User uservice.Userer
	Auth aservice.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	userService := uservice.NewUserService(storages.User, components.Logger)
	return &Services{
		User: userService,
		Auth: aservice.NewAuth(userService, storages.Verify, components),
	}
}
