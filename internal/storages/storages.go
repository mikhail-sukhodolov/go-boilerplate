package storages

import (
	"boilerplate/internal/db/adapter"
	"boilerplate/internal/infrastructure/cache"
	vstorage "boilerplate/internal/modules/auth/storage"
	ustorage "boilerplate/internal/modules/user/storage"
)

type Storages struct {
	User   ustorage.Userer
	Verify vstorage.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		User:   ustorage.NewUserStorage(sqlAdapter, cache),
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
